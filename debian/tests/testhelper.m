function testhelper(ext, iface)
    pkg load io

    ## Do not load libreoffice/UNO stuff (the 3rd arg is omitted), since UNO does
    ## not work in a chroot
    chk_spreadsheet_support("/usr/share/java", 0);

    if nargin < 2
        iface = [];
    endif
    fname = [ "debian/tests/test." ext ];
    xlsfinfo(fname, iface);
    d = xlsread(fname);
    assert(size(d) == [ 1001, 2]);
endfunction
