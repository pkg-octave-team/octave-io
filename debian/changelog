octave-io (2.6.4-3) unstable; urgency=medium

  * Workaround for version mismatch between apache-poi and xmlbeans (#1012016)
    - xmlbeans-versioned.patch: new patch that requires xmlbeans version 3
      (which is not in Debian, so in practice this means that the POI+OOXML
      interface will be disabled, unless xmlbeans 3 is manually installed)
    - disable the xlsx-poi autopkgtest
  * README.Debian: misc improvements
  * Bump Standards-Version to 4.6.2, no changes needed

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 01 Mar 2023 17:46:30 +0100

octave-io (2.6.4-2) unstable; urgency=medium

  * Team upload

  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * No need for cleaning *-tst files
    + d/clean: Drop line with src/*.cc-tst
    + d/control: Build-depend on dh-octave >= 1.2.2. In this version
      dh_octave_clean removes the src/*-tst files.
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector
  * d/copyright: Update copyright years for debian/* files
  * d/s/lintian-overrides: Add override for source-is-missing

 -- Rafael Laboissière <rafael@debian.org>  Tue, 06 Dec 2022 11:47:11 -0300

octave-io (2.6.4-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch

  [ Rafael Laboissière ]
  * d/copyright: Accentuate my family name
  * d/control: Bump Standards-Version to 4.6.0 (no changes needed)
  * d/clean: Remove the *-tst leftover files
  * Set upstream metadata fields: Archive.

  [ Sébastien Villemot ]
  * New upstream version 2.6.4
  * d/copyright: reflect upstream changes

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 04 Jan 2022 15:02:12 +0100

octave-io (2.6.3-1) unstable; urgency=medium

  * New upstream version 2.6.3
  * d/copyright: reflect upstream changes
  * bug59273.patch: drop patch, applied upstream
  * conditional-java.patch: new patch, avoids FTBFS when Octave lacks Java
    support or Java is not installed

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 04 Nov 2020 11:26:31 +0100

octave-io (2.6.2-2) unstable; urgency=medium

  * bug59273.patch: new patch from upstream, fixes the autopkgtests

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 22 Oct 2020 13:00:32 +0200

octave-io (2.6.2-1) unstable; urgency=medium

  * Team upload

  * New upstream version 2.6.2
  * d/p/dbfread-big-endian.patch: Drop patch (applied upstream)

 -- Rafael Laboissière <rafael@debian.org>  Sat, 17 Oct 2020 04:32:46 -0300

octave-io (2.6.1-3) unstable; urgency=medium

  * Team upload.

  * d/p/commons-compress-unversioned.patc: Set Forwarded:not-needed

 -- Rafael Laboissière <rafael@debian.org>  Sun, 02 Aug 2020 12:32:39 -0300

octave-io (2.6.1-2) unstable; urgency=medium

  * Team upload.

  * d/u/metadata: New file
  * d/control: Bump debhelper compatibility level to 13
  * d/p/dbfread-big-endian.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Thu, 28 May 2020 04:43:55 -0300

octave-io (2.6.1-1) unstable; urgency=medium

  * New upstream version 2.6.1

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 18 Apr 2020 14:22:58 +0200

octave-io (2.6.0-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.6.0
  * d/copyright: reflect upstream changes
  * Documentation is now in a single HTML file
  * Adapt autopkgtest for deprecation of odsfinfo and odsread
  * commons-compress-unversioned.patch: new patch, needed for POI+OOXML interface

  [ Rafael Laboissière ]
  * d/control: Bump Standards-Version to 4.5.0 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 26 Mar 2020 15:24:29 +0100

octave-io (2.4.13-2) unstable; urgency=medium

  * Team upload.

  * d/control: Bump dependency on dh-octave to >= 0.7.1
    This allows the injection of the virtual package octave-abi-N into the
    package's list of dependencies

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 06 Nov 2019 03:10:30 -0300

octave-io (2.4.13-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.4.13
  * d/copyright: reflect upstream changes
  * d/copyright: use correct license short name (FSFAP) for Makefile
  * Drop unused source override for autopkgtest field

  [ Rafael Laboissiere ]
  * d/control: Bump Standards-Version to 4.4.1 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 25 Oct 2019 23:43:55 +0200

octave-io (2.4.12-2) unstable; urgency=medium

  * Team upload.

  * d/control:
    + Bump Standards-Version to 4.3.0 (no changes needed)
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat
  * d/t/control: Renamed from d/t/control.autodep8
  * d/s/lintian-overrides: Add override for Litian tag
    unnecessary-testsuite-autopkgtest-field

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:56:19 -0200

octave-io (2.4.12-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.4.12
  * d/copyright: reflect upstream changes
  * Combine auto-generated autopkgtests with manually specified ones.
    Add "Testsuite: autopkgtest-pkg-octave" and rename d/t/control to
    d/t/control.autodep8.

  [ Rafael Laboissiere ]
  * d/control: Add Rules-Requires-Root: no
  * d/control: Bump Standards-Version to 4.2.1 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 17 Nov 2018 10:06:46 +0100

octave-io (2.4.11-1) unstable; urgency=medium

  * New upstream version 2.4.11
  * d/copyright: reflect upstream changes.
  * Bump S-V to 4.1.4.
  * Drop drop-version-number-check-for-outdated-versions-of-JRE.patch.
    No longer needed.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 08 May 2018 10:29:45 +0200

octave-io (2.4.10-3) unstable; urgency=medium

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Sébastien Villemot ]
  * drop-version-number-check-for-outdated-versions-of-JRE.patch: new patch.
    Thanks to Mike Miller (Closes: #894348)
  * Allow stderr messages in autopkgtest, needed for Apache POI since the move
    to Java 9.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 05 Apr 2018 11:05:35 +0200

octave-io (2.4.10-2) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control: Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:32:05 -0200

octave-io (2.4.10-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.4.10
  * d/copyright: reflect upstream changes.
  * Remove Testsuite: autopkgtest-pkg-octave.
    This is unneeded since there is already a custom testsuite.
  * d/clean: remove files generated by configure script.

  [ Rafael Laboissiere ]
  * d/control: Use Debian's GitLab URLs in Vcs-* headers

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 16 Jan 2018 11:47:34 +0100

octave-io (2.4.8-2) unstable; urgency=medium

  * Team upload.

  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:42 -0200

octave-io (2.4.8-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.4.8
  * d/copyright: reflect upstream changes.
  * Remove unnecessary versioned depends on libreoffice-core and
    libxerces2-java.

  [ Rafael Laboissiere ]
  * d/control: Bump Standards-Version to 4.1.1 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 27 Oct 2017 14:09:18 +0200

octave-io (2.4.7-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.4.7
  * d/p/ods-oct.patch: drop patch, applied upstream.
  * d/copyright:
    + Reflect upstream changes.
    + Use secure URL for format.
  * d/watch: bump to format version 4.

  [ Rafael Laboissiere ]
  * d/control: Use cgit instead of gitweb in Vcs-Browser URL

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 09 Jun 2017 16:38:02 +0200

octave-io (2.4.5-1) unstable; urgency=medium

  * New upstream version 2.4.5
  * d/p/ods-oct.patch: new patch, fixes reading of ODS with OCT interface.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 26 Dec 2016 13:41:08 +0100

octave-io (2.4.4-1) unstable; urgency=medium

  * New upstream version 2.4.4
  * d/copyright: reflect upstream changes.
  * Use autopkgtest for running xls(x)/oct/gnumeric reading tests.
  * d/control: add missing default-jre-headless to Recommends.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 06 Nov 2016 22:34:04 +0100

octave-io (2.4.3-1) unstable; urgency=medium

  * New upstream version 2.4.3
  * d/copyright: reflect upstream changes.
  * Bump to debhelper compat level 10.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 19 Sep 2016 00:48:31 +0200

octave-io (2.4.2-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Imported Upstream version 2.4.2
  * d/copyright: reflect upstream changes.
  * d/p/uno.patch: drop patch, no more needed with recent LO.

  [ Rafael Laboissiere ]
  * Bump Standards-Version to 3.9.8 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 14 Jul 2016 16:42:02 +0200

octave-io (2.4.1-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Imported Upstream version 2.4.1
  * d/copyright: reflect upstream changes.

  [ Rafael Laboissiere ]
  * d/control: Use secure URIs in the Vcs-* fields
  * d/control: Bump Standards-Version to 3.9.7 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 27 Mar 2016 21:17:38 +0200

octave-io (2.4.0-1) unstable; urgency=medium

  * Imported Upstream version 2.4.0
  * d/copyright: reflect upstream changes.
  * no-flexml.patch: drop patch, no longer needed.
  * Activate JOpenDocument interface (library now packaged in Debian).
    + Add libjopendocument-java to B-D and Recommends.
    + Mention the interface in README.Debian.
    + Add an interface-specific test to debian/check.m.
  * Add libxerces2-java to Recommends, used by xml{read,write}.m.
  * Register READ-{ODS,XLS}.html to doc-base.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 20 Jan 2016 14:40:23 +0100

octave-io (2.2.11-1) unstable; urgency=medium

  * Imported Upstream version 2.2.11

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 26 Oct 2015 14:36:19 +0100

octave-io (2.2.10-1) unstable; urgency=medium

  * Imported Upstream version 2.2.10
  * d/copyright: reflect upstream changes.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 01 Oct 2015 22:25:49 +0200

octave-io (2.2.9-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Imported Upstream version 2.2.9
  * Bump dependency on octave-pkg-dev to 1.3.1.
    Needed to avoid a spurious octave-Octave in Depends field.

  [ Rafael Laboissiere ]
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)
  * d/copyright: Reflect upstream changes
  * d/p/uno.patch: Refresh for new upstream version
  * d/NEWS: Use regular paragraphs

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 11 Sep 2015 08:47:52 +0200

octave-io (2.2.4-1) unstable; urgency=medium

  * Imported Upstream version 2.2.4
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 18 Sep 2014 16:02:29 +0200

octave-io (2.2.3-1) unstable; urgency=medium

  * Imported Upstream version 2.2.3
  * Remove Thomas Weber from Uploaders.

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 15 Aug 2014 15:37:40 +0200

octave-io (2.2.2-1) unstable; urgency=medium

  * Imported Upstream version 2.2.2

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 28 May 2014 16:24:45 +0200

octave-io (2.2.1-1) unstable; urgency=medium

  * Imported Upstream version 2.2.1
  * Since 2.2.0, java libraries are no longer autoloaded. Therefore:
    - mention this in NEWS.Debian
    - explain in README.Debian how to manually load java libraries
    - explicitly run chk_spreadsheet_support in testsuite, but without UNO
      support since it does not work in chroots (and therefore drop
      build-dependencies on libreoffice)
    - drop low-priority-for-UNO.patch, since it is now less relevant
  * Drop low-priority-for-POI.patch, no longer needed (#739602 was fixed in
    2.2.0-1).
  * Simplify uno.patch.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 30 Apr 2014 13:11:07 +0200

octave-io (2.2.0-1) unstable; urgency=low

  * Imported Upstream version 2.2.0
    + New version can successfully load Gnumeric files via the OCT interface
      (closes: #739632)
  * Refresh patches
  * Add dep3 description to uno.patch

 -- Thomas Weber <tweber@debian.org>  Tue, 22 Apr 2014 11:54:30 +0200

octave-io (2.0.2-1) unstable; urgency=low

  * Imported Upstream version 2.0.2
  * debian/copyright: reflect upstream changes.
  * Build-depend on octave 3.8.
  * Remove dependencies on octave-java, which is now part of octave core.
  * Update patches:
    + configure-java-classpatch.patch: no longer needed
    + uno.patch: new patch, fixes detection of UNO interface
    + low-priority-for-UNO.patch: new patch, lowers priority of UNO interface
  * Add a testsuite for {xls,ods}read on various example files.
  * Update Recommends for interfaces:
    + OCT needs unzip
    + UNO now needs libreoffice-core instead of ure

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 20 Feb 2014 16:03:00 +0100

octave-io (1.2.5-1) unstable; urgency=low

  * Imported Upstream version 1.2.5
  * debian/copyright: reflect upstream changes
  * Recommend libapache-poi-java.
    That package now contains the JARs necessary for the POI+OOXML interface.
  * low-priority-for-POI.patch: new patch.
    Puts the POI interface at the tail of the interface priority list. The
    interface does not seem to work correctly for now.
  * README.Debian: update information about POI and OCT interfaces.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 16 Nov 2013 15:11:56 +0100

octave-io (1.2.4-1) unstable; urgency=low

  * Imported Upstream version 1.2.4
  * debian/copyright: reflect upstream changes.
  * Bump Standards-Version to 3.9.5, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 06 Nov 2013 13:54:20 +0000

octave-io (1.2.3-2) unstable; urgency=low

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

  [ Sébastien Villemot ]
  * no-flexml.patch: also remove the .l file in the patch.
    This should fix the occasional FTBFS on some autobuilders.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 27 Aug 2013 14:55:50 +0200

octave-io (1.2.3-1) unstable; urgency=low

  * Imported Upstream version 1.2.3

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 16 Aug 2013 23:01:06 +0200

octave-io (1.2.2-1) unstable; urgency=low

  * Imported Upstream version 1.2.2
  * debian/copyright: reflect upstream changes
  * autoload-yes.patch: refresh patch
  * private-testsuite.patch: remove patch (upstream disabled tests in private
    functions)

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 26 May 2013 10:07:48 +0200

octave-io (1.2.1-2) unstable; urgency=low

  * Upload to unstable

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 18 May 2013 14:59:56 +0200

octave-io (1.2.1-1) experimental; urgency=low

  * Imported Upstream version 1.2.1
  * debian/copyright: reflect upstream changes
  * configure-java-classpath.patch: refresh patch
  * autoload-yes.patch: new patch, forces autoloading of package
  * private-testsuite.patch: new patch, deals with tests under private/
  * Bump Standards-Version to 3.9.4, no changes needed
  * Use my @debian.org email address
  * Remove obsolete DM-Upload-Allowed flag
  * Add Build-Conflicts with octave-java.
    If octave-java is installed, the testsuite fails because PKG_ADD tries to
    load chk_spreadsheet_support, which is not yet in the path at that moment.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 17 Apr 2013 11:39:52 +0200

octave-io (1.0.19-1) unstable; urgency=low

  * Imported Upstream version 1.0.19

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sun, 17 Jun 2012 08:12:01 +0000

octave-io (1.0.18-2) unstable; urgency=medium

  * configure-java-classpath.patch: update patch to avoid breakage of
    octave-java

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sat, 09 Jun 2012 15:22:18 +0200

octave-io (1.0.18-1) unstable; urgency=low

  * Imported Upstream version 1.0.18
  * debian/README.Debian: more details about spreadsheet formats supported
  * Change architecture to "any" and add ${shlibs:Depends}
    Compiled code has been reintroduced in the package
  * debian/control: recommend LibreOffice URE >= 3.5
  * debian/watch: use SourceForge redirector
  * debian/copyright: reflect upstream changes
  * debian/patches/configure-java-classpath.patch: refresh patch
  * debian/patches/no-flexml.patch: new patch

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Mon, 02 Apr 2012 23:10:25 +0200

octave-io (1.0.17-1) unstable; urgency=low

  [ Thomas Weber ]
  * Change architecture to "all". The packages doesn't contain any compiled
    code anymore.

  [ Sébastien Villemot ]
  * Imported Upstream version 1.0.17
  * Remove ${shlibs:Depends}, meaningless for an arch:all package
  * Bump to debhelper compat level 9
  * Build-depend on octave-pkg-dev >= 1.0.1, to compile against Octave 3.6
    (Closes: #649395)
  * Add Sébastien Villemot to Uploaders
  * Bump to Standards-Version 3.9.3, no changes needed
  * Include HTML documentation in the package
  * debian/copyright: upgrade to machine-readable format 1.0
  * Recommend octave-java, libreoffice-java-common, ure, libjexcelapi-java
  * debian/patches/configure-java-classpath.patch: new patch
  * Add README.Debian about spreadsheet support
  * debian/control: mention OpenDocument spreadsheet in long description

 -- Thomas Weber <tweber@debian.org>  Tue, 20 Mar 2012 00:36:43 +0100

octave-io (1.0.14-2) unstable; urgency=low

  * Upload to unstable

 -- Thomas Weber <tweber@debian.org>  Mon, 11 Apr 2011 22:31:56 +0200

octave-io (1.0.14-1) experimental; urgency=low

  * New upstream release

 -- Thomas Weber <tweber@debian.org>  Sun, 10 Apr 2011 17:54:54 +0200

octave-io (1.0.13-1) experimental; urgency=low

  * New upstream release

 -- Thomas Weber <tweber@debian.org>  Thu, 02 Sep 2010 22:33:31 +0200

octave-io (1.0.12-1) unstable; urgency=low

  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571920)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Bump Standards-Version to 3.8.4 (no changes needed)
  * Switch to dpkg-source 3.0 (quilt) format

 -- Thomas Weber <tweber@debian.org>  Thu, 13 May 2010 22:19:55 +0200

octave-io (1.0.9-1) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

  [ Thomas Weber ]
  * New upstream release
  * Drop patch dlmwrite-succeed-test.diff, applied upstream
  * Add README.source describing our quilt usage.

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Tue, 29 Dec 2009 17:56:40 +0100

octave-io (1.0.8-1) unstable; urgency=low

  * New upstream release
  * patches/dlmwrite-succeed-test.diff: New patch for making the test of
    dlmwrite succeed
  * debian/control:
    + (Standards-Version): Bump to 3.8.1 (no changes needed)
    + (Depends): Add ${misc:Depends}
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
    + (Build-Depends): Drop quilt
  * debian/copyright:
    + Use DEP5 URL in Format-Specification
    + Use separate License stanzas for instructing about the location of
      the different licenses used in the package

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 14:42:32 +0200

octave-io (1.0.7-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * Upload to unstable
  * debian/control, debian/rules: Add quilt support
  * debian/patches/only-add-once-to-delim: New patch, fix a test failure

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 05 Apr 2009 18:47:00 +0200

octave-io (1.0.7-1) experimental; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * debian/control: Bumped Standards-Version to 3.8.0 (no changes
    needed)

  [ Thomas Weber ]
  * New upstream release
  * Bump dependency on octave-pkg-dev to 0.6.1, to get the experimental
    version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Tue, 09 Dec 2008 21:25:15 +0100

octave-io (1.0.6-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * New upstream version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 12 May 2008 10:08:37 +0200

octave-io (1.0.5-1) unstable; urgency=low

  * Initial release (closes: #468506)

 -- Ólafur Jens Sigurðsson <ojsbug@gmail.com>  Sun, 10 Feb 2008 18:12:10 +0100
